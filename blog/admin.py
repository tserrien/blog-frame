from django.contrib import admin
from . import models
#from .models import Post, Comment

class CommentInline(admin.StackedInline):
    model = models.Comment


class PostAdmin(admin.ModelAdmin):
    inlines = [
    CommentInline,
    ]

# Register your models here.
admin.site.register(models.Post, PostAdmin)
admin.site.register(models.Comment)
